package com.example.fabiopicchi.textcrashingapp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    ArrayList<PlaceholderFragment> fragments = new ArrayList<>();
    ArrayList<FragmentData> datas = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        datas.clear();
        fragments.clear();
        // Checks the orientation of the screen
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            datas.add(new FragmentData("BOM", "DIA"));
            datas.add(new FragmentData("BOA", "TARDE"));
            datas.add(new FragmentData("BOA", "NOITE"));
        } else {
            datas.add(new FragmentData("GOOD", "MORNING"));
            datas.add(new FragmentData("GOOD", "AFTERNOON"));
            datas.add(new FragmentData("GOOD", "EVENING"));
        }

        for(FragmentData data : datas){
            fragments.add(PlaceholderFragment.newInstance(data));
        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragments);
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        datas.clear();
        // Checks the orientation of the screen
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            datas.add(new FragmentData("BOM", "DIA"));
            datas.add(new FragmentData("BOA", "TARDE"));
            datas.add(new FragmentData("BOA", "NOITE"));
        } else {
            datas.add(new FragmentData("GOOD", "MORNING"));
            datas.add(new FragmentData("GOOD", "AFTERNOON"));
            datas.add(new FragmentData("GOOD", "EVENING"));
        }

        for (int i = 0; i < datas.size(); i++) {
            fragments.get(i).onOrientationChange(datas.get(i));
        }

    }


    public interface OrientationChange{
        public void onOrientationChange(FragmentData data);
    }
}
