package com.example.fabiopicchi.textcrashingapp;

import java.io.Serializable;

/**
 * Created by fabiopicchi on 10/20/15.
 */
public class FragmentData implements Serializable{
    private String s1;
    private String s2;

    public FragmentData(String s1, String s2){
        this.s1 = s1;
        this.s2 = s2;
    }

    public String getS1(){
        return s1;
    }

    public String getS2(){
        return s2;
    }
}
