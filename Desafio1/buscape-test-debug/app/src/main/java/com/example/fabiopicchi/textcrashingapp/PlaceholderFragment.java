package com.example.fabiopicchi.textcrashingapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by fabiopicchi on 10/20/15.
 */
public class PlaceholderFragment extends Fragment {
    private FragmentData data;

    public static PlaceholderFragment newInstance(FragmentData data) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        fragment.data = data;
        return fragment;
    }

    public PlaceholderFragment() {
    }

    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText(String.format("%s %s", data.getS1(), data.getS2()));
        return rootView;
    }

    public void onOrientationChange(FragmentData data) {
        this.data = data;
        textView.setText(String.format("%s %s", data.getS1(), data.getS2()));
    }
}