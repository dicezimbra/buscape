package com.cezimbra.tutorialapp;

import java.io.Serializable;

/**
 * Created by cezimbra on 16/08/2016.
 */
public interface ViewPagerClick extends Serializable {
    public void setPage(int page);
    public void nextPage();
}
