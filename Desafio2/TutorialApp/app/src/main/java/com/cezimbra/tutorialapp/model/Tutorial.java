package com.cezimbra.tutorialapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cezimbra on 11/08/2016.
 */
public class Tutorial implements Serializable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("detailsURL")
    @Expose
    private String detailsURL;

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     *     The detailsURL
     */
    public String getDetailsURL() {
        return detailsURL;
    }

    /**
     *
     * @param detailsURL
     *     The detailsURL
     */
    public void setDetailsURL(String detailsURL) {
        this.detailsURL = detailsURL;
    }
}
