package com.cezimbra.tutorialapp.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.cezimbra.tutorialapp.R;
import com.cezimbra.tutorialapp.ViewPagerClick;
import com.cezimbra.tutorialapp.adapter.FragmentPageAdapter;
import com.cezimbra.tutorialapp.controller.ApiEndpointInterface;
import com.cezimbra.tutorialapp.controller.ApplicationController;
import com.cezimbra.tutorialapp.controller.NetComponent;
import com.cezimbra.tutorialapp.model.BackendTutorial;
import com.cezimbra.tutorialapp.model.BackendTutorialDetail;
import com.cezimbra.tutorialapp.model.Tutorial;
import com.cezimbra.tutorialapp.model.TutorialDetail;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends BaseActivity implements Callback<BackendTutorialDetail>, ViewPagerClick {

    public static final String EXTRA_TUTORIAL = "tuto_extras";

    @InjectView(R.id.vpPager)
    ViewPager vpPager;

    @Inject
    ApiEndpointInterface backend;

    List<PageFragment> mFragments = new ArrayList<>();
    FragmentPageAdapter mAdapter;

    @Override
    public void setPage(int page) {
        vpPager.setCurrentItem(page);
    }

    @Override
    public void nextPage() {
        if(vpPager.getCurrentItem() +1 == mFragments.size()){
            this.finish();
        }else{
            vpPager.setCurrentItem(vpPager.getCurrentItem() + 1);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setupToolbar();
        injectViews();

        Tutorial tutorial = (Tutorial) getIntent().getExtras().getSerializable(EXTRA_TUTORIAL);

        mAdapter = new FragmentPageAdapter(getSupportFragmentManager(), mFragments);

        vpPager.setAdapter(mAdapter);


        String url = tutorial.getDetailsURL();
        url = url.replace(ApplicationController.IP, "");

        Call<BackendTutorialDetail> call = backend.getTutorialDetail(url);
        call.enqueue(this);
    }

    @Override
    protected void performInjection(NetComponent component) {
        component.inject(this);
    }

    @Override
    public void onResponse(Call<BackendTutorialDetail> call, Response<BackendTutorialDetail> response) {

        mFragments.clear();
        List<TutorialDetail> details = response.body().getContent().getTourItems();

        for (int i = 0; i < details.size(); i++) {

            String text = "Próximo";
            if(details.size() == (i+1)){
                text = "Finalizar";
            }

            PageFragment fragment = PageFragment.newInstance(details.get(i), this, text);
            mFragments.add(fragment);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onFailure(Call<BackendTutorialDetail> call, Throwable t) {
        t.printStackTrace();
    }
}
