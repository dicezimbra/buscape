package com.cezimbra.tutorialapp.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.cezimbra.tutorialapp.R;
import com.cezimbra.tutorialapp.controller.ApplicationController;
import com.cezimbra.tutorialapp.controller.NetComponent;

import butterknife.ButterKnife;

/**
 * Created by cezimbra on 11/08/2016.
 */
public abstract class BaseFragment extends Fragment {

    public void injectViews(Fragment frag, View v, Activity act){
        ButterKnife.inject( frag, v);
        performInjection(ApplicationController.from(act).getComponent(), v);
    }

    public ApplicationController getApp(Activity act){
        return (ApplicationController) act.getApplication();
    }

    protected abstract void performInjection(NetComponent component, View v);


}
