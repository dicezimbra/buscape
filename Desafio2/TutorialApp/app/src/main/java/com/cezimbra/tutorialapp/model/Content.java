
package com.cezimbra.tutorialapp.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Content {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("tourItems")
    @Expose
    private List<TutorialDetail> tourItems = new ArrayList<TutorialDetail>();
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("pagingColor")
    @Expose
    private String pagingColor;
    @SerializedName("selectedPagingItemColor")
    @Expose
    private String selectedPagingItemColor;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The tourItems
     */
    public List<TutorialDetail> getTourItems() {
        return tourItems;
    }

    /**
     * 
     * @param tourItems
     *     The tourItems
     */
    public void setTourItems(List<TutorialDetail> tourItems) {
        this.tourItems = tourItems;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The pagingColor
     */
    public String getPagingColor() {
        return pagingColor;
    }

    /**
     * 
     * @param pagingColor
     *     The pagingColor
     */
    public void setPagingColor(String pagingColor) {
        this.pagingColor = pagingColor;
    }

    /**
     * 
     * @return
     *     The selectedPagingItemColor
     */
    public String getSelectedPagingItemColor() {
        return selectedPagingItemColor;
    }

    /**
     * 
     * @param selectedPagingItemColor
     *     The selectedPagingItemColor
     */
    public void setSelectedPagingItemColor(String selectedPagingItemColor) {
        this.selectedPagingItemColor = selectedPagingItemColor;
    }

}
