package com.cezimbra.tutorialapp.controller;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by cezimbra on 11/08/2016.
 */
public class ApplicationController extends Application {

    static NetComponent mNetComponent;
    static public String IP = "https://dl.dropboxusercontent.com/";
    @Override
    public void onCreate() {
        super.onCreate();

        mNetComponent = DaggerNetComponent.builder()
                .applicationModule(new ApplicationModule(this, IP))
                .build();
    }
    public static ApplicationController from(@NonNull Context context) {
        return (ApplicationController) context.getApplicationContext();
    }

    public static NetComponent getComponent() {
        return mNetComponent;
    }

}
