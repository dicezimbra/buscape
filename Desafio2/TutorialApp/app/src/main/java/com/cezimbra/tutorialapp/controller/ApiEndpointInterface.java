package com.cezimbra.tutorialapp.controller;

import com.cezimbra.tutorialapp.model.BackendTutorial;
import com.cezimbra.tutorialapp.model.BackendTutorialDetail;
import com.cezimbra.tutorialapp.model.Tutorial;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by cezimbra on 11/08/2016.
 */

public interface ApiEndpointInterface {

    @GET("u/1337551/teste_buscape/json/list.json")
    Call<BackendTutorial> getTutorials(@Header("Device-Density") String density);

    @GET("{url}")
    Call<BackendTutorialDetail> getTutorialDetail(@Path("url") String url);

}
