package com.cezimbra.tutorialapp.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.cezimbra.tutorialapp.R;
import com.cezimbra.tutorialapp.controller.ApiEndpointInterface;
import com.cezimbra.tutorialapp.controller.NetComponent;
import com.cezimbra.tutorialapp.model.BackendTutorial;
import com.cezimbra.tutorialapp.model.Tutorial;
import com.cezimbra.tutorialapp.adapter.TutorialAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements Callback<BackendTutorial> {

    @Inject
    ApiEndpointInterface backend;

    @InjectView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView.Adapter mAdapter;

    List<Tutorial> mTutorialList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        injectViews();

        mTutorialList = new ArrayList<>();

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new TutorialAdapter(mTutorialList, MainActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        Call<BackendTutorial> call = backend.getTutorials(getString(R.string.density));
        call.enqueue(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<BackendTutorial> call, Response<BackendTutorial> response) {

        mTutorialList.clear();
        mTutorialList.addAll(response.body().getList());

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onFailure(Call<BackendTutorial> call, Throwable t) {
        t.printStackTrace();
    }

    @Override
    protected void performInjection(NetComponent component) {
        component.inject(this);
    }
}
