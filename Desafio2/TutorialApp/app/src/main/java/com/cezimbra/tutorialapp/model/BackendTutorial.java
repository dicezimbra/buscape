
package com.cezimbra.tutorialapp.model;

import java.util.ArrayList;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackendTutorial {

    @SerializedName("return")
    @Expose
    private String _return;
    @SerializedName("list")
    @Expose
    private java.util.List<Tutorial> list = new ArrayList<Tutorial>();

    /**
     * 
     * @return
     *     The _return
     */
    public String getReturn() {
        return _return;
    }

    /**
     * 
     * @param _return
     *     The return
     */
    public void setReturn(String _return) {
        this._return = _return;
    }

    /**
     * 
     * @return
     *     The list
     */
    public java.util.List<Tutorial> getList() {
        return list;
    }

    /**
     * 
     * @param list
     *     The list
     */
    public void setList(java.util.List<Tutorial> list) {
        this.list = list;
    }

}
