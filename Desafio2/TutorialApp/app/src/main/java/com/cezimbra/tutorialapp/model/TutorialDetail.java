
package com.cezimbra.tutorialapp.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class TutorialDetail implements Serializable {

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("titleColor")
    @Expose
    private String titleColor;
    @SerializedName("textColor")
    @Expose
    private String textColor;

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 
     * @return
     *     The background
     */
    public String getBackground() {
        return background;
    }

    /**
     * 
     * @param background
     *     The background
     */
    public void setBackground(String background) {
        this.background = background;
    }

    /**
     * 
     * @return
     *     The titleColor
     */
    public String getTitleColor() {
        return titleColor;
    }

    /**
     * 
     * @param titleColor
     *     The titleColor
     */
    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    /**
     * 
     * @return
     *     The textColor
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * 
     * @param textColor
     *     The textColor
     */
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

}
