package com.cezimbra.tutorialapp.view;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cezimbra.tutorialapp.R;
import com.cezimbra.tutorialapp.ViewPagerClick;
import com.cezimbra.tutorialapp.controller.NetComponent;
import com.cezimbra.tutorialapp.model.TutorialDetail;
import com.squareup.picasso.Picasso;

import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * to handle interaction events.
 * Use the {@link PageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageFragment extends BaseFragment {

    public PageFragment() {

    }

    private TutorialDetail mTutorialDetail;
    private ViewPagerClick mPagerClick;
    private String mButtonText;

    @InjectView(R.id.text)
    TextView text;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.image)
    ImageView image;

    @InjectView(R.id.background)
    View background;

    @InjectView(R.id.line)
    View click;

    @InjectView(R.id.next)
    TextView next;

    public static final String DETAIL = "detail";
    public static final String PAGER_CLICK = "pager";
    public static final String TEXT = "text";
    public static PageFragment newInstance(TutorialDetail detail, ViewPagerClick viewPagerClick, String text) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putSerializable(DETAIL, detail);
        args.putSerializable(PAGER_CLICK, viewPagerClick);
        args.putString(TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mTutorialDetail = (TutorialDetail) getArguments().getSerializable(DETAIL);
            this.mPagerClick = (ViewPagerClick) getArguments().getSerializable(PAGER_CLICK);
            this.mButtonText = getArguments().getString(TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_page, container, false);

        injectViews(this, v, getActivity());

        text.setText(mTutorialDetail.getText());
        text.setTextColor(Color.parseColor(mTutorialDetail.getTextColor()));

        title.setText(mTutorialDetail.getTitle());
        title.setTextColor(Color.parseColor(mTutorialDetail.getTitleColor()));

        if(!mTutorialDetail.getBackground().contains("#")){
            mTutorialDetail.setBackground("#" + mTutorialDetail.getBackground());
        }

        background.setBackgroundColor(Color.parseColor(mTutorialDetail.getBackground()));

        Picasso.with(getActivity())
                .load(mTutorialDetail.getImage())
                .into(image);


        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPagerClick.nextPage();
            }
        });

        next.setText(mButtonText);

        return v;
    }

    @Override
    protected void performInjection(NetComponent component, View v) {
        component.inject(this);
    }



}
