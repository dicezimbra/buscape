package com.cezimbra.tutorialapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cezimbra.tutorialapp.R;
import com.cezimbra.tutorialapp.model.Tutorial;
import com.cezimbra.tutorialapp.view.DetailsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by cezimbra on 11/08/2016.
 */
public class TutorialAdapter extends RecyclerView.Adapter {

    List<Tutorial> mTutorialList;
    private Context mContext;
    public TutorialAdapter(List<Tutorial> tutorialList, Context context) {
        this.mTutorialList = tutorialList;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_tutorial, parent, false);
        TutorialHolder vHolder = new TutorialHolder(view);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TutorialHolder tutorialHolder = (TutorialHolder) holder;

        final Tutorial tutorial = mTutorialList.get(position);
        tutorialHolder.description.setText(tutorial.getDescription());

        Log.e("TutorialApp", "TutorialAdapter onBindViewHolder tutorial.getDescription9() = "+tutorial.getDescription());

        tutorialHolder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mContext, DetailsActivity.class);
                it.putExtra(DetailsActivity.EXTRA_TUTORIAL, tutorial);
                mContext.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTutorialList.size();
    }

    public static class TutorialHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.description)
        TextView description;
        @InjectView(R.id.click)
        View click;

        public TutorialHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
