package com.cezimbra.tutorialapp.controller;

import android.view.View;

import com.cezimbra.tutorialapp.view.BaseActivity;
import com.cezimbra.tutorialapp.view.DetailsActivity;
import com.cezimbra.tutorialapp.view.MainActivity;
import com.cezimbra.tutorialapp.view.PageFragment;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Provides;

/**
 * Created by cezimbra on 11/08/2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface NetComponent {
    void inject(MainActivity activity);
    void inject(DetailsActivity activity);
    void inject(PageFragment activity);
}
