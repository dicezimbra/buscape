package com.cezimbra.tutorialapp.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.cezimbra.tutorialapp.R;
import com.cezimbra.tutorialapp.controller.ApplicationController;
import com.cezimbra.tutorialapp.controller.NetComponent;

import butterknife.ButterKnife;

/**
 * Created by cezimbra on 11/08/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void injectViews(){
        ButterKnife.inject(this);
        performInjection(ApplicationController.from(this).getComponent());
    }

    public ApplicationController getApp(){
        return (ApplicationController) getApplication();
    }

    protected abstract void performInjection(NetComponent component);


}
