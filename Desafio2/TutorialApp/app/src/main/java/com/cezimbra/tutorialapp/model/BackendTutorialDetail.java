
package com.cezimbra.tutorialapp.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BackendTutorialDetail {

    @SerializedName("return")
    @Expose
    private String _return;
    @SerializedName("content")
    @Expose
    private Content content;

    /**
     * 
     * @return
     *     The _return
     */
    public String getReturn() {
        return _return;
    }

    /**
     * 
     * @param _return
     *     The return
     */
    public void setReturn(String _return) {
        this._return = _return;
    }

    /**
     * 
     * @return
     *     The content
     */
    public Content getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    public void setContent(Content content) {
        this.content = content;
    }

}
