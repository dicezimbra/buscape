package com.cezimbra.tutorialapp.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.cezimbra.tutorialapp.ViewPagerClick;
import com.cezimbra.tutorialapp.view.PageFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cezimbra on 16/08/2016.
 */
public class FragmentPageAdapter extends FragmentPagerAdapter {

    List<PageFragment> mFragments;
    public FragmentPageAdapter(FragmentManager fm, List<PageFragment> fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }
}
